apply plugin: 'com.android.library'
apply plugin: 'jacoco'
apply plugin: 'com.github.kt3k.coveralls'

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:4.0.0'
    }
}

jacoco {
    toolVersion = "0.8.0"
}

android {
    compileSdkVersion androidCompileSdkVersion
    buildToolsVersion androidBuildToolsVersion

    defaultPublishConfig !isReleaseBuild() ? "debug" : "release"

    defaultConfig {
        minSdkVersion androidMinSdkVersion
        targetSdkVersion androidTargetSdkVersion
        versionCode Integer.parseInt(project.VERSION_CODE)
        versionName project.VERSION_NAME

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles "consumer-rules.pro"
        multiDexEnabled true
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }

        debug {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    dexOptions {
        javaMaxHeapSize "4g"
    }

    lintOptions {
        abortOnError false
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
        coreLibraryDesugaringEnabled true
    }

    packagingOptions {
        exclude 'LICENSE.txt'
        exclude 'META-INF/*'
    }

    testOptions {
        unitTests {
            includeAndroidResources = true
            returnDefaultValues = true
        }
    }
}

tasks.withType(Test) {
    jacoco.includeNoLocationClasses = true
}

allprojects {
    repositories {
        maven { url 'https://oss.sonatype.org/content/repositories/snapshots/' }
        maven { url "https://s3.amazonaws.com/repo.commonsware.com" }
        mavenLocal()
    }
}

tasks.withType(Test) {
    jacoco.includeNoLocationClasses = true
}

dependencies {
    implementation fileTree(dir: "libs", include: ["*.jar"])
    implementation 'androidx.appcompat:appcompat:1.1.0'

    api 'org.smartregister:opensrp-client-core:1.15.3-SNAPSHOT'
    api ('org.smartregister:opensrp-client-native-form:1.13.2-SNAPSHOT') {
        transitive = true
        exclude group: 'org.smartregister', module: 'opensrp-client-simprints'
        exclude group: 'com.ibm.fhir', module: 'fhir-model'
        exclude group: 'org.smartregister', module: 'opensrp-client-core'
    }

    implementation('org.smartregister:opensrp-client-configurable-views:1.1.5-SNAPSHOT@aar') {
        transitive = true
        exclude group: 'org.smartregister', module: 'opensrp-client-core'
        exclude group: 'com.android.support', module: 'appcompat-v7'
    }

    implementation 'org.smartregister:opensrp-client-reporting:0.0.6-RVL-SNAPSHOT@aar'

    implementation ('io.ona.kujaku:library:0.8.3') {
        transitive = true
        exclude group: 'com.android.volley'
        exclude group: 'stax', module: 'stax-api'
    }
    implementation 'xerces:xercesImpl:2.12.0'

    coreLibraryDesugaring 'com.android.tools:desugar_jdk_libs:1.0.5'
    implementation 'com.mapbox.mapboxsdk:mapbox-sdk-turf:5.1.0'


    implementation 'com.google.android:flexbox:1.0.0'
    implementation 'com.google.android.material:material:1.0.0'


    // Test dependencices
    testImplementation 'junit:junit:4.13'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'

    testImplementation 'org.mockito:mockito-inline:2.13.0'
    testImplementation 'org.robolectric:robolectric:4.2'
    testImplementation 'org.robolectric:shadows-multidex:4.2'
    // PowerMock
    def powerMockVersion = '2.0.0'
    testImplementation "org.powermock:powermock-module-junit4:$powerMockVersion"
    testImplementation "org.powermock:powermock-module-junit4-rule:$powerMockVersion"
    testImplementation "org.powermock:powermock-api-mockito2:$powerMockVersion"
    testImplementation "org.powermock:powermock-classloading-xstream:$powerMockVersion"

}

task jacocoTestReport(type: JacocoReport, dependsOn: ['testDebugUnitTest']) {

    reports {
        xml.enabled = true
        html.enabled = true
    }

    getReports().getXml().setDestination(file("${buildDir}/reports/jacoco/jacocoRootReport/merged.xml"))
    getReports().getHtml().setDestination(file("${buildDir}/reports/jacoco/jacocoRootReport/html"))

    def fileFilter = ['**/R.class', '**/R$*.class', '**/BuildConfig.*', '**/Manifest*.*', '**/*Test*.*', 'android/**/*.*', '**/*$ViewBinder*.*']
    def debugTree = fileTree(dir: "$project.buildDir/intermediates/javac/debug/classes", excludes: fileFilter)
    def mainSrc = "$project.projectDir/src/main/java"

    sourceDirectories.setFrom(files([mainSrc]))
    classDirectories.setFrom(files([debugTree]))
    executionData.setFrom(fileTree(dir: project.buildDir, includes: [
            'jacoco/testDebugUnitTest.exec', 'outputs/code-coverage/connected/*coverage.ec'
    ]))
}
coveralls {
    jacocoReportPath = "${buildDir}/reports/jacoco/jacocoRootReport/merged.xml"
}

apply from: '../maven.gradle'

task javadoc(type: Javadoc) {
    failOnError false
    def mainSrc = "$project.projectDir/src/main/java"
    source = files([mainSrc])
    classpath += project.files(android.getBootClasspath().join(File.pathSeparator))
    classpath += configurations.compile
}
